<?php
/**
* @Author : Allan Bicol
* @To Do : Import CSV function and command
*/

namespace App\Command;
use App\Entity\TblProductData;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class CsvImportCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();

        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('csv:import')
            ->addArgument('mode',InputArgument::OPTIONAL,'import mode')
            ->setDescription('Imports a mock CSV file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $io = new SymfonyStyle($input, $output);

        $io->title('Attempting to import the feed...');

        //Read CSV file using League\Csv
        $reader = Reader::createFromPath('%kernel.root_dir%/../src/Data/stock.csv', 'r');

        $results = $reader->fetchAssoc();

        $io->progressStart(iterator_count($results));

        $countImportSuccess = 0;
        $countImportFail = 0;
        $failResultData = [];

        //Loop all read results from CSV
        foreach ($results as $row){

            //Check Product Code to make sure no duplicates
            $product = $this->em->getRepository(TblProductData::class)
                ->findOneBy([
                        'strProductCode' => $row['Product Code']
                ]);

            //insert data if not duplicate entry
            if(null === $product){
            
                //check if cost is greated than 5 and less that 1000 and Stock is greater than 10
                if((float)$row['Cost in GBP'] > 5 && (float)$row['Cost in GBP'] < 1000 && (int)$row['Stock'] > 10){
                    $product = new TblProductData();
                    $product->setStrProductName($row['Product Name']);
                    $product->setStrProductDesc($row['Product Description']);
                    $product->setStrProductCode($row['Product Code']);
                    $product->setDtmAdded(new \DateTime());
                    $product->setIntStock((int) $row['Stock']);
                    $product->setFltCostGBP((float) $row['Cost in GBP']);
                    $product->setStmTimestamp(new \DateTime());

                    //if Discontinued field id marked as Yes
                    if($row['Discontinued'] !== ''){
                        $product->setDtmDiscontinued(new \DateTime());
                    }

                    $this->em->persist($product);

                    //Check if mode argument is test or not: if not test then save data to database
                    if($input->getArgument('mode') != 'test'){
                        $this->em->flush();
                    }

                    $countImportSuccess += 1;

                }else{
                    $failResultData[] = array('Product Code' => $row['Product Code'],
                                            'Product Name' => $row['Product Name'],
                                            'Product Desc' => $row['Product Description'],
                                            'Stock' => $row['Stock'],
                                            'Cost in GBP' => $row['Cost in GBP'],
                                        );
                    $countImportFail += 1;
                }

            }

            $io->progressAdvance();
        }

        $io->progressFinish();

        //Check if mode argument is test or not: if not test then save data to database
        if($input->getArgument('mode') != 'test'){
            $this->em->flush();
        }

        //Success display of how many items was successfully imported
        $io->success($countImportSuccess .' Items was successfully imported.');

        //Error display of how many items failed to import
        $io->error($countImportFail .' Items was failed to import.');
        $io->title('List of items which fail to be inserted');
        $io->table(array('Product Code','Product Name','Product Desc','Stock','Cost in GBP'),$failResultData);
        

    }
}