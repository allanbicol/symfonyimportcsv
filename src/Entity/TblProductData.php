<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tblProductData")
 * @ORM\Entity(repositoryClass="App\Repository\TblProductDataRepository")
 */
class TblProductData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="intProductDataId", type="integer", length=10)
     */
    private $intProductDataId;

    /**
     * @ORM\Column(name="strProductDataName",type="string", length=50)
     */
    private $strProductName;

    /**
     * @ORM\Column(name="strProductDataDesc",type="string", length=255)
     */
    private $strProductDesc;

    /**
     * @ORM\Column(name="strProductDataCode",type="string", length=10)
     */
    private $strProductCode;

    /**
     * @ORM\Column(name="dtmAdded",type="datetime", nullable=true)
     */
    private $dtmAdded;

    /**
     * @ORM\Column(name="dtmDiscontinued",type="datetime", nullable=true)
     */
    private $dtmDiscontinued;

    /**
     * @ORM\Column(name="intStock",type="integer", nullable=true)
     */
    private $intStock;

    /**
     * @ORM\Column(name="fltCostGBP",type="float", nullable=true)
     */
    private $fltCostGBP;


    /**
     * @ORM\Column(name="stmTimestamp",type="datetime", nullable=true)
     */
    private $stmTimestamp;

    
    public function getIntProductDataId()
    {
        return $this->intProductDataId;
    }

    public function setIntProductDataId(int $intProductDataId)
    {
        $this->intProductDataId = $intProductDataId;

        return $this;
    }

    public function getStrProductName()
    {
        return $this->strProductName;
    }

    public function setStrProductName(string $strProductName)
    {
        $this->strProductName = $strProductName;

        return $this;
    }

    public function getStrProductDesc()
    {
        return $this->strProductDesc;
    }

    public function setStrProductDesc(string $strProductDesc)
    {
        $this->strProductDesc = $strProductDesc;

        return $this;
    }

    public function getStrProductCode()
    {
        return $this->strProductCode;
    }

    public function setStrProductCode(string $strProductCode)
    {
        $this->strProductCode = $strProductCode;

        return $this;
    }

    public function getDtmAdded()
    {
        return $this->dtmAdded;
    }

    public function setDtmAdded(\DateTime $dtmAdded)
    {
        $this->dtmAdded = $dtmAdded;

        return $this;
    }

    public function getDtmDiscontinued()
    {
        return $this->dtmDiscontinued;
    }

    public function setDtmDiscontinued(\DateTime $dtmDiscontinued)
    {
        $this->dtmDiscontinued = $dtmDiscontinued;

        return $this;
    }

    public function getIntStock()
    {
        return $this->intStock;
    }

    public function setIntStock(int $intStock)
    {
        $this->intStock = $intStock;

        return $this;
    }

    public function getFltCostGBP()
    {
        return $this->fltCostGBP;
    }

    public function setFltCostGBP(float $fltCostGBP)
    {
        $this->fltCostGBP = $fltCostGBP;

        return $this;
    }

    public function getStmTimestamp()
    {
        return $this->stmTimestamp;
    }

    public function setStmTimestamp(\DateTime $stmTimestamp)
    {
        $this->stmTimestamp = $stmTimestamp;

        return $this;
    }
}
